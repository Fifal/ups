package cz.filek.ups.controllers;

import cz.filek.ups.Main;
import cz.filek.ups.config.Config;
import cz.filek.ups.network.TCP;
import cz.filek.ups.threads.ClientListenerThread;
import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.Separator;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.stage.Stage;

import java.io.IOException;
import java.net.InetAddress;
import java.net.URL;
import java.net.UnknownHostException;
import java.util.ResourceBundle;

import static java.lang.Thread.sleep;

/**
 * Created by Fífal on 4. 10. 2016.
 */
public class LoginController implements Initializable {
    @FXML
    private Button btnLogin;

    @FXML
    private ImageView imageView;

    @FXML
    private Separator separator;

    @FXML
    private Label lblInfo;

    @FXML
    private TextField txtIPAddress;
    @FXML
    private TextField txtPort;
    @FXML
    private TextField txtNickName;

    private TCP tcp;
    private ClientListenerThread clientListenerThread;
    private Stage primaryStage;
    public boolean logged = false;


    @Override
    public void initialize(URL location, ResourceBundle resources) {
        if (Config.DEBUG_GUI) {
            createGameUI();
            showGameUI();
        } else {
            if (Config.DEBUG_LOGIN) {
                txtIPAddress.setText(Config.DEBUG_IP);
                txtPort.setText(Config.DEBUG_PORT);
                txtNickName.setText(Config.DEBUG_NICK);
            }
            btnLogin.setOnAction(event -> login());
            btnLogin.getStyleClass().add("material-button");

            txtIPAddress.getStyleClass().add("material-text-field");
            txtPort.getStyleClass().add("material-text-field");
            txtNickName.getStyleClass().add("material-text-field");

            separator.getStyleClass().add("material-separator");

            imageView.setImage(new Image(getClass().getResource("/layouts/guesserino.png").toString()));
        }
    }

    private void login() {
        setLblInfo("");
        int port;
        try{
            port = Integer.parseInt(txtPort.getText());
            if(port <1 || port > 65535){
                setLblInfo("Chyba, špatně zadán port.");
                return;
            }
        }catch (Exception ex){
            setLblInfo("Chyba, špatně zadán port.");
            return;
        }

        try {
            InetAddress inetAddress = InetAddress.getByName(txtIPAddress.getText());

            tcp = new TCP(inetAddress, port);
            Config.TCP_CONNECTION = tcp;
            createGameUI();

            if (tcp.getSocket() != null) {
                clientListenerThread = new ClientListenerThread(tcp);
                Config.CLIENT_LISTENER_RUNNING = true;
                Thread thread = new Thread(clientListenerThread);
                thread.start();
                sleep(100);
                tcp.loginUser(txtNickName.getText());

                new Thread(){
                    @Override
                    public void run(){
                        try {
                            sleep(10000);
                        } catch (InterruptedException e) {
                            //e.printStackTrace();
                        }
                        if(!logged){
                            Config.CLIENT_LISTENER_RUNNING = false;
                            try {
                                if(tcp != null && tcp.getSocket() != null){
                                    tcp.getSocket().close();
                                }
                            } catch (IOException e) {
                                //e.printStackTrace();
                            }
                            resetTcp();
                            setLblInfo("Server neodpověděl včas.");
                        }
                    }
                }.start();
            }
            else{
                setLblInfo("Chyba při připojování k serveru.");
                return;
            }


        } catch (UnknownHostException e) {
            setLblInfo("Chyba, špatně zadaná adresa.");
        } catch (InterruptedException e) {
            //e.printStackTrace();
        }
    }

    private void createGameUI() {
        try {
            primaryStage = new Stage();
            FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/layouts/GUI.fxml"));
            Parent root = fxmlLoader.load();
            primaryStage.setTitle(Config.WINDOW_TITLE);
            Scene scene = new Scene(root, Config.WINDOW_WIDTH, Config.WINDOW_HEIGHT);
            scene.getStylesheets().add(getClass().getResource("/styles/main.css").toString());
            primaryStage.setScene(scene);
            primaryStage.setResizable(false);
            Config.FXMLLOADER = fxmlLoader;
        } catch (Exception ex) {

        }
    }

    /**
     * Closes login form and open form with game
     */
    public void showGameUI() {
        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                Config.IS_GUI_SHOWN = true;
                primaryStage.show();
                Main.hideWindow();
                primaryStage.setOnCloseRequest(event -> {
                    Config.CLIENT_LISTENER_RUNNING = false;
                    Config.IS_GUI_SHOWN = false;
                    try {
                        tcp.getSocket().close();
                    } catch (IOException e) {
                    }
                    resetTcp();
                    showLoginUI();
                });
            }
        });
    }

    public void showLoginUI(){
        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                if(Main.isWindowHidden()) {
                    setLblInfo("");
                    Main.showWindow();
                }
            }
        });
    }

    public void closeGameUI(){
        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                primaryStage.hide();
            }
        });
    }

    /**
     * Sets label with info from server
     *
     * @param message string
     */
    public void setLblInfo(String message) {
        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                lblInfo.setVisible(true);
                lblInfo.setText(message);
            }
        });
    }

    public void resetTcp() {
        this.tcp = null;
    }

    public String getNickname() {
        return txtNickName.getText();
    }
}
