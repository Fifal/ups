package cz.filek.ups.controllers;

import cz.filek.ups.config.Config;
import cz.filek.ups.network.Message;
import cz.filek.ups.network.NetworkCode;
import cz.filek.ups.network.NetworkCodes;
import cz.filek.ups.network.TCP;
import cz.filek.ups.threads.MessageSenderThread;
import cz.filek.ups.utils.ImageUtils;
import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Cursor;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.*;
import javafx.scene.image.WritableImage;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.paint.Paint;
import javafx.scene.shape.StrokeLineCap;
import javafx.scene.shape.StrokeLineJoin;

import java.net.URL;
import java.util.ResourceBundle;

public class GuiController implements Initializable {
    private TCP tcpConnection;
    private MessageSenderThread messageSenderThread;
    @FXML
    private Canvas canvas;
    private GraphicsContext graphicsContext;

    @FXML
    private Pane canvasPane;

    @FXML
    private Label lblDrawing;

    @FXML
    private TextArea txtInfo; // Chat

    @FXML
    private Button btnSend;
    @FXML
    private Button btnClear; // Button clears canvas

    @FXML
    private Button btnGuess;
    @FXML
    private TextField txtGuess;

    @FXML
    private Button btnReady;

    @FXML
    private Label lblPlayer;
    @FXML
    private Label lblScore;

    @FXML
    private ColorPicker colorPicker;

    public boolean wasCleared = false;
    public boolean imageSent = false;
    public boolean guessSent = false;
    public boolean guessSendInProgress = false;


    @Override
    public void initialize(URL location, ResourceBundle resources) {

        tcpConnection = Config.TCP_CONNECTION;

        messageSenderThread = new MessageSenderThread(tcpConnection);

        Thread thread = new Thread(messageSenderThread);
        thread.start();

        colorPicker.setValue(Color.valueOf("#000000"));
        colorPicker.getStyleClass().add("material-color-picker");

        // Canvas stuff
        canvas = new Canvas(Config.CANVAS_WIDTH, Config.CANVAS_HEIGHT);

        // Green border around canvas
        canvasPane.getChildren().add(canvas);
        canvasPane.setMaxWidth(Config.CANVAS_WIDTH);
        canvasPane.setMaxHeight(Config.CANVAS_HEIGHT);
        canvasPane.getStyleClass().add("material-canvas");

        graphicsContext = canvas.getGraphicsContext2D();
        clearCanvas();
        canvas.setOnMousePressed(event -> {
            graphicsContext.beginPath();
            graphicsContext.moveTo(event.getX(), event.getY());
        });

        canvas.setCursor(Cursor.HAND);

        canvas.setOnMouseClicked(event -> {
            graphicsContext.setFill(colorPicker.getValue());
            graphicsContext.fillOval(event.getX() - 2.5, event.getY() - 2.5, 5, 5);
        });

        canvas.setOnMouseDragged(event -> {
            graphicsContext.lineTo(event.getX(), event.getY());
            graphicsContext.setLineWidth(5);
            graphicsContext.setLineJoin(StrokeLineJoin.ROUND);
            graphicsContext.setLineCap(StrokeLineCap.ROUND);
            graphicsContext.setStroke(colorPicker.getValue());
            graphicsContext.stroke();
        });

        canvas.setOnMouseReleased(event -> graphicsContext.closePath());


        // TextArea
        txtInfo.setEditable(false);
        txtInfo.setWrapText(true);
        txtInfo.textProperty().addListener(new ChangeListener<Object>() {
            @Override
            public void changed(ObservableValue<?> observable, Object oldValue,
                                Object newValue) {
                txtInfo.setScrollTop(Double.MAX_VALUE);
            }
        });
        txtInfo.getStyleClass().add("material-text-area");

        // Buttons
        btnClear.setOnAction(event -> clearCanvas());
        btnClear.getStyleClass().add("material-button-red");

        btnSend.setOnAction(event -> sendImage());
        btnSend.getStyleClass().add("material-button");

        btnReady.setOnAction(event -> {
            Message message = new Message("", NetworkCode.CL_READY);
            messageSenderThread.addMessage(message);
            btnReady.setDisable(true);
        });
        btnReady.getStyleClass().add("material-button");

        btnGuess.setOnAction(event -> {
                sendGuess();
        });
        btnGuess.getStyleClass().add("material-button");

        // TextField
        txtGuess.getStyleClass().add("material-text-field");
        setButtons(false);

        // Label drawing
        lblDrawing.setText("Vítejte ve hře.");
    }

    /**
     * Clears canvas with white color
     */
    private void clearCanvas() {
        Paint paint = Paint.valueOf("#FFFFFF");
        graphicsContext.setFill(paint);
        graphicsContext.fillRect(0, 0, canvas.getWidth(), canvas.getHeight());
    }

    /**
     * Sends Image to a server
     */
    private void sendImage() {
        WritableImage writableImage = canvas.snapshot(null, null);
        byte[] imgArray = ImageUtils.getByteArrayFromImg(writableImage);
        String imgString = ImageUtils.getStringFromByteArray(imgArray);
        Message message = new Message(imgString, NetworkCode.CL_IMAGE);
        messageSenderThread.addMessage(message);

        btnSend.setDisable(true);
        btnClear.setDisable(true);
        wasCleared = false;
    }

    private void sendGuess() {
        Message message = new Message(txtGuess.getText(), NetworkCode.CL_GUESS);
        if (message.toString().split(":").length == 4 && !txtGuess.getText().contains(";") && !txtGuess.getText().contains(":") && !txtGuess.getText().contains("\n")) {
            messageSenderThread.addMessage(message);
        } else {
            setInfoArea("Hádané slovo obsahuje nepovolené znaky!");
        }
        txtGuess.setText("");
    }

    /**
     * Sets GUI buttons depending on if client has to draw image or guess
     *
     * @param drawing if set to true, user can draw
     */
    public void setButtons(boolean drawing) {
        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                if (!wasCleared) {
                    clearCanvas();
                    wasCleared = true;
                }
                if (drawing) {
                    btnSend.setDisable(false);
                    btnClear.setDisable(false);
                    canvas.setDisable(false);

                    btnGuess.setDisable(true);
                    txtGuess.setDisable(true);
                } else {
                    btnSend.setDisable(true);
                    btnClear.setDisable(true);
                    canvas.setDisable(true);

                    btnGuess.setDisable(false);
                    txtGuess.setDisable(false);
                }
            }
        });
    }

    /**
     * Sets label drawing text
     *
     * @param message string
     */
    public void setLblDrawing(String message) {
        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                lblDrawing.setText(message);
            }
        });
    }

    /**
     * @param imgString
     */
    public void setCanvasImage(String imgString) {
        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                byte[] resultArray = ImageUtils.getByteArrayFromString(imgString);
                WritableImage result = ImageUtils.getImgFromByteArray(resultArray);
                canvas.getGraphicsContext2D().drawImage(result, 0, 0);
            }
        });
    }

    public void setInfoArea(String info) {
        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                txtInfo.setText(txtInfo.getText() + info + "\n");
                txtInfo.appendText("");
            }
        });
    }

    public void setPlayerInfo(String info) {
        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                lblPlayer.setText(info);
            }
        });
    }

    public void setScore(String score) {
        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                lblScore.setText("Skóre: " + score);
            }
        });
    }

    public MessageSenderThread getMessageSender(){
        return messageSenderThread;
    }
}
