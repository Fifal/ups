package cz.filek.ups.threads;

import cz.filek.ups.config.Config;
import cz.filek.ups.controllers.GuiController;
import cz.filek.ups.controllers.LoginController;
import cz.filek.ups.network.NetworkCode;
import cz.filek.ups.network.NetworkCodes;
import cz.filek.ups.network.TCP;
import cz.filek.ups.utils.ConsoleLogger;

import java.io.IOException;

import static java.lang.Thread.sleep;


/**
 * Created by fifal on 19.10.16.
 */
public class ClientListenerThread implements Runnable {
    private TCP tcpInfo;
    private LoginController loginController;
    private GuiController guiController;

    public ClientListenerThread(TCP tcpInfo) {
        this.tcpInfo = tcpInfo;
        loginController = (LoginController) Config.FXMLLOADER_LOGIN.getController();
        guiController = (GuiController) Config.FXMLLOADER.getController();
    }

    @Override
    public void run() {
        while (Config.CLIENT_LISTENER_RUNNING) {
            try {
                if (tcpInfo.getSocket() == null) {
                    Config.CLIENT_LISTENER_RUNNING = false;
                }
                String message = tcpInfo.receiveMsg();
                ConsoleLogger.info("[ClientListenerThread][run]: Přijata zpráva: "+message);
                if (message != null && !message.equals("")) {
                    if (isValidMessage(message)) {
                        processMessage(message);
                    }
                }
            } catch (Exception ex) {
                ConsoleLogger.debug("[ClientListenerThread][run]: " +ex.getMessage());
            }
        }
        ConsoleLogger.info("[ClientListenerThread][run]: Končím vlákno pro listener.");
    }

    /**
     * Validates received message
     *
     * @param message string
     * @return true if message is valid
     */
    private boolean isValidMessage(String message) {
        message = message.replace(";", "");
        String splittedMessage[] = message.split(":");
        if (splittedMessage.length < 2) {
            return false;
        }
        if (NetworkCodes.getNetworkCode(splittedMessage[1]) == NetworkCode.NOT_NETWORK_CODE) {
            return false;
        }
        return true;
    }

    /**
     * Processes the message
     *
     * @param message string
     */
    private void processMessage(String message) throws IOException {
        message = message.replace(";", "");
        String[] splittedMessage = message.split(":");
        NetworkCode networkCode = NetworkCodes.getNetworkCode(splittedMessage[1]);

        if (networkCode != NetworkCode.SRV_ACK
                && networkCode!= NetworkCode.SRV_NOT_VALID
                && networkCode!= NetworkCode.SRV_DROPPED) {
            try {
                long hash = Long.parseLong(splittedMessage[splittedMessage.length - 1]);
                tcpInfo.sendMsg(NetworkCodes.PROTOCOL_PREFIX + NetworkCodes.getMessage(NetworkCode.CL_ACK) + hash + NetworkCodes.PROTOCOL_SUFFIX);
            } catch (Exception ex) {
                ConsoleLogger.debug("[ClientListenerThread][processMessage]: Špatný hash: " + ex.getMessage());
            }
        }

        switch (networkCode) {
            case SRV_SUCCESSFULLY_LOGGED:
                showGameUI();
                setPlayerInfo("Přihlášen: " + loginController.getNickname());
                setScore("0");
                loginController.logged = true;
                break;
            case SRV_NAME_TAKEN:
                setLoginInfo("Uživatelské jméno již existuje.");
                loginController.resetTcp();
                Config.CLIENT_LISTENER_RUNNING = false;
                break;
            case SRV_NOT_FOUND:
                Config.CLIENT_LISTENER_RUNNING = false;
                Thread.currentThread().interrupt();
                break;
            case SRV_IMAGE:
                setCanvasImage(splittedMessage[2]);
                break;
            case SRV_DRAW:
                setDrawing(true);
                break;
            case SRV_WORD:
                guiController.wasCleared = false;
                setLblDrawing("Slovo k nakreslení je: " + splittedMessage[2]);
                break;
            case SRV_BTNS_GUESS:
                setLblDrawing("Kreslí jiný hráč.");
                setDrawing(false);
                break;
            case SRV_INFO:
                splittedMessage[2] = splittedMessage[2].replace("<>", "\n");
                splittedMessage[2] = splittedMessage[2].replace("><", ":");
                setInfoArea(splittedMessage[2]);
                break;
            case SRV_SCORE:
                setScore(splittedMessage[2]);
                break;
            case SRV_DROPPED:
                if (Config.IS_GUI_SHOWN) {
                    setInfoArea("\n\n !!![KLIENT]: Ztraceno spojení se serverem. Po 5 vteřinách budete přesunuti na přihlašovací obrazovku.\n\n");
                    try {
                        sleep(5000);
                    } catch (Exception ex) {
                        ConsoleLogger.debug("[ClientListenerThread][processMessage]: " + ex.getMessage());
                    }
                    loginController.showLoginUI();
                    loginController.closeGameUI();
                }

                Config.CLIENT_LISTENER_RUNNING = false;

                loginController.resetTcp();
                break;
            case SRV_BAD_NAME:
                setLoginInfo("Chyba, počet znaků jména <1; 15>.");
                break;
            case SRV_SERVER_FULL:
                setLoginInfo("Chyba server je plný.");
                break;
            case SRV_ACK:
                long hash = Long.parseLong(splittedMessage[splittedMessage.length-1]);
                guiController.getMessageSender().ackMessage(hash);
            default:
                break;
        }
    }

    /**
     * Sets text on info label in login form
     *
     * @param message string
     */
    private void setLoginInfo(String message) {
        loginController.setLblInfo(message);
    }

    /**
     * Closes login form and open game UI
     */
    private void showGameUI() {
        loginController.showGameUI();
    }

    /**
     * Sets label what word to draw
     *
     * @param info string
     */
    private void setLblDrawing(String info) {
        guiController.setLblDrawing(info);
    }

    private void setDrawing(boolean drawing) {
        guiController.setButtons(drawing);
    }

    private void setCanvasImage(String imgString) {
        guiController.setCanvasImage(imgString);
    }

    private void setInfoArea(String info) {
        guiController.setInfoArea(info);
    }

    private void setPlayerInfo(String info) {
        guiController.setPlayerInfo(info);
    }

    private void setScore(String score) {
        guiController.setScore(score);
    }
}
