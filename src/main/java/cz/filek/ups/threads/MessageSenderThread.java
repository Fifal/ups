package cz.filek.ups.threads;

import cz.filek.ups.config.Config;
import cz.filek.ups.network.Message;
import cz.filek.ups.network.TCP;
import cz.filek.ups.utils.ConsoleLogger;

import java.util.ArrayList;

import static java.lang.Thread.sleep;

/**
 * @author Filip Jani on 18.1.17.
 */
public class MessageSenderThread implements Runnable {
    private TCP tcpInfo;
    private ArrayList<Message> messages;

    public MessageSenderThread(TCP tcp) {
        this.tcpInfo = tcp;
        this.messages = new ArrayList<>();
    }

    public void ackMessage(long hashCode) {
        ConsoleLogger.info("[MessageSenderThread][ackMessage]: Potvrzuji zprávu s hashem: " + hashCode);
        for (int i = 0; i < messages.size(); i++) {
            if (messages.get(i).getHashCode() == hashCode) {
                messages.get(i).confirm();
                ConsoleLogger.info("[MessageSenderThread][ackMessage]: Zpráva potvrzena: " + messages.get(i).toString());
            }
        }
    }

    public void addMessage(Message message) {
        messages.add(message);
        tcpInfo.sendMsg(message.toString());
    }

    @Override
    public void run() {
        while (Config.CLIENT_LISTENER_RUNNING) {
            try {
                sleep(1);
            } catch (InterruptedException e) {
                ConsoleLogger.debug("[MessageSenderThread][run]: " + e.getMessage());
            }

            for (int i = 0; i < messages.size(); i++) {
                if (!messages.get(i).isConfirmed() && (System.currentTimeMillis() > (messages.get(i).getSentTime() + 5000))) {
                    tcpInfo.sendMsg(messages.get(i).toString());
                    try {
                        sleep(50);
                    } catch (InterruptedException e) {
                        ConsoleLogger.debug("[MessageSenderThread][run]: " + e.getMessage());
                    }
                    messages.get(i).updateSentTime();
                }
            }
        }
    }
}
