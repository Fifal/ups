package cz.filek.ups.config;


import cz.filek.ups.network.TCP;
import javafx.fxml.FXMLLoader;

/**
 * Created by Fífal on 2. 10. 2016.
 */
public final class Config {
    // Debuging
    public static boolean DEBUG_GUI = false; // If set to true, application will not need login into a server
    public static boolean DEBUG_LOGIN = true;
    public static String DEBUG_IP = "localhost";
    public static String DEBUG_PORT = "20001";
    public static String DEBUG_NICK = "fifal";

    // Window settings
    public static String WINDOW_TITLE = "Guesserino";
    public static int WINDOW_WIDTH = 820;
    public static int WINDOW_HEIGHT = 500;

    public static FXMLLoader FXMLLOADER;
    public static FXMLLoader FXMLLOADER_LOGIN;

    // Canvas settings
    public static int CANVAS_WIDTH = 384;
    public static int CANVAS_HEIGHT = 384;

    // Threads
    public static boolean CLIENT_LISTENER_RUNNING = true;

    // TCP
    public static TCP TCP_CONNECTION;

    public static boolean IS_GUI_SHOWN = false;
}
