package cz.filek.ups.network;

import cz.filek.ups.config.Config;
import cz.filek.ups.controllers.LoginController;
import cz.filek.ups.utils.ConsoleLogger;

import java.net.*;
import java.io.*;

import static java.lang.Thread.sleep;

/**
 * Created by fifal on 9.10.16.
 */
public class TCP {
    private InetAddress IPAddress;
    private int port;
    private Socket socket;

    public TCP(InetAddress IPAddress, int port) {
        this.IPAddress = IPAddress;
        this.port = port;

        Thread socketThread = new Thread() {
            public void run() {
                try {
                    socket = new Socket(IPAddress, port);
                } catch (Exception e) {
                    LoginController loginController = (LoginController) Config.FXMLLOADER_LOGIN.getController();
                    loginController.setLblInfo("Chyba při připojování k serveru.");
                    ConsoleLogger.info("[TCP][konstruktor]: Chyba při připojování k serveru.");
                }
            }
        };
        socketThread.start();
        try {
            socketThread.join(1500);
        } catch (InterruptedException e) {
            LoginController loginController = (LoginController) Config.FXMLLOADER_LOGIN.getController();
            loginController.setLblInfo("Chyba při připojování k serveru.");
            ConsoleLogger.info("[TCP][konstruktor]: Chyba při připojování k serveru.");
        }
    }


    /**
     * Login user
     *
     * @param nickName nick
     * @return 0 if login was sucessful
     */
    public int loginUser(String nickName){
        String connString = NetworkCodes.PROTOCOL_PREFIX
                + NetworkCodes.getMessage(NetworkCode.CL_CONNECT) +":"+ nickName
                + ":" + (NetworkCodes.getMessage(NetworkCode.CL_CONNECT) +":"+ nickName).hashCode() + NetworkCodes.PROTOCOL_SUFFIX;
        sendMsg(connString);
        return 0;
    }


    /**
     * Sends message to server
     *
     * @param data message
     */
    public void sendMsg(String data){
        try {
            if (socket != null) {
                DataOutputStream dataOutputStream = new DataOutputStream(socket.getOutputStream());
                dataOutputStream.write(data.getBytes());
                ConsoleLogger.info("[TCP][sendMsg]: Odesílám zprávu: " + data);
                sleep(50);
            }
        } catch (IOException e) {
            ConsoleLogger.debug("[TCP][sendMsg]: " + e.getMessage());
        } catch (InterruptedException e) {
            ConsoleLogger.debug("[TCP][sendMsg]: " + e.getMessage());
        }
    }

    /**
     * Receives message from server
     *
     * @return message
     */
    public String receiveMsg() {
        try {
            if (socket != null) {
                InputStream is = socket.getInputStream();
                InputStreamReader isr = new InputStreamReader(is);
                BufferedReader br = new BufferedReader(isr);
                String buffer;
                String message = "";
                while ((buffer = br.readLine()) != null) {
                    message += buffer;
                    if (message.contains(";")) {
                        return message;
                    }
                }
                return NetworkCodes.PROTOCOL_PREFIX + NetworkCodes.getMessage(NetworkCode.SRV_DROPPED);
            } else {
                return NetworkCodes.PROTOCOL_PREFIX + NetworkCodes.getMessage(NetworkCode.SRV_NOT_FOUND) + NetworkCodes.PROTOCOL_SUFFIX;
            }
        } catch (IOException e) {
            if(e.getMessage().equals("Connection reset")){
                Config.CLIENT_LISTENER_RUNNING = false;
                return NetworkCodes.PROTOCOL_PREFIX + NetworkCodes.getMessage(NetworkCode.SRV_DROPPED);
            }
            return "";
        }
    }

    public Socket getSocket() {
        return socket;
    }
}
