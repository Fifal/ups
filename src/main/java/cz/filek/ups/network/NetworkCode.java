package cz.filek.ups.network;

/**
 * Created by fifal on 19.10.16.
 */
public enum NetworkCode {
    CL_CONNECT, // Code for connecting to server
    CL_DISCONNECT, // Disconnecting from server
    CL_IMAGE, // Sending image to server
    CL_GUESS, // User is guessing
    CL_READY, // User is ready
    CL_ACK,
    SRV_SUCCESSFULLY_LOGGED, // Response from server, successfully logged
    SRV_NAME_TAKEN,  // Name is already taken
    SRV_SERVER_FULL, // Server is full
    SRV_IMAGE, // Server is sending image
    SRV_NOT_FOUND, // If server not responding
    SRV_DRAW, // Client has to draw image
    SRV_WORD, // Word to draw
    SRV_BAD_NAME,
    SRV_ACK,
    SRV_BTNS_GUESS,
    SRV_INFO,
    SRV_SCORE,
    SRV_DISCONNECT,
    SRV_DROPPED,
    SRV_NOT_VALID,
    NOT_NETWORK_CODE // Unknown network code
}
