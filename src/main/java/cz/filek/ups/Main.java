package cz.filek.ups;

import cz.filek.ups.config.Config;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class Main extends Application {
    private static Stage ps;
    private static boolean isHidden = false;

    @Override
    public void start(Stage primaryStage) throws Exception{
        ps = new Stage();
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/layouts/Login.fxml"));
        Parent root = fxmlLoader.load();
        ps.setTitle(Config.WINDOW_TITLE + "Login");
        Scene scene = new Scene(root, 450, 400);
        scene.getStylesheets().add(getClass().getResource("/styles/main.css").toString());
        ps.setScene(scene);
        ps.setResizable(false);
        Config.FXMLLOADER_LOGIN = fxmlLoader;
        ps.show();
    }

    public static void main(String[] args) {
        launch(args);
    }

    public static void hideWindow(){
        ps.hide();
        isHidden = true;
    }

    public static void showWindow(){
        ps.show();
        isHidden = false;
    }

    public static boolean isWindowHidden(){
        return isHidden;
    }
}
